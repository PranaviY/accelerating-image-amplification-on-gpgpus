# README #

Image Amplification
Last Edited: 28th February, 2017

This is the MAIN folder containing the most optimized code for Image Amplification on the GPGPU device. This version maps all of the
algorithm stages (Canny edge detection, vertical edge-keeping, horizontal edge-keeping, and mean-keeping) to the GPGPU device. A detailed description
of algorithm mapping appears in:

Yalamanchili, P., Italia, P., Murray, A., Pallipuram V.K. (2017). Submitted: Journal of Supercomputing.

System-specific setup:
1)Load the g++ complier and CUDA Toolkit 7.5 ["https://developer.nvidia.com/cuda-75-downloads-archive"].

2)make file generates the executable "amplify"
> make clean all

3) Run the executable with the necessary inputs:
> ./amplify <path to image> <mask size> <sigma> <alpha>
Example: ./amplify ../LENNA/Lenna_org_1024.pgm 11 1.1 2

**Auxilliary Folders** 

The folders Level 1 through Level 2 correspond to the implementation hierarchy developed by Pranavi Yalamanchili for her Master's Thesis titled,
"A multi-level implementation of image amplification on the GPGPU device".

#LEVEL-1
This folder consists of code that performs the Canny Edge detection alone on the GPGPU device. The remainder of the algorithm stages are performed
on the CPU host.
#LEVEL-2
This folder consists of code that performs the Canny Edge detection and Vertical Edge Keeping on the GPGPU device and rest all the steps on the CPU host.
#LEVEL-3
This folder consists of code that performs Canny Edge detection,Vertical Edge Keeping, and Horizontal Edge Keeping on the GPGPU device and rest of the stages on the CPU host.
#LEVEL-4
This folder consists of code that performs Canny Edge detection,Vertical Edge Keeping,Horizontal Edge Keeping and a naive version of mean keeping on GPGPU and rest all the stages on the CPU host.

For more information, contact:
Corresponding Author: Dr. V.K. Pallipuram
vpallipuramkrishnamani@pacific.edu

First Author:
Pranavi Yalamanchili
p_yalamanchili@u.pacific.edu