#define BLOCKWIDTH 16
#define g_w 11
#define apron 3
#define BUFFER 512

//void convolve_naive(float *h_image,int h_im_width,int h_im_height,float *h_mask,int h_m_w,float *h_outimage);
//void convolve_impl1(float *h_image,int h_im_width,int h_im_height,float *h_mask,int h_m_w,float *h_outimage);
//void convolve_shared(float *h_image,int h_im_width,int h_im_height,float *h_mask,int h_m_w,float *h_outimage);
//void consolidated_convolve(float *h_image,int h_im_width,int h_im_height,float *h_gx_mask,float *h_gy_mask,int h_m_w,float *h_IGy_outimage,float *h_IGx_outimage);
//void consolidated_convolveMagAngle(float *Gxy,float *h_image,int h_im_width,int h_im_height,float *h_gx_mask,float *h_gy_mask,int h_m_w,float *h_Gxy_outimage,float *h_I_angle);
//void consolidated_convolveMagAngleSuppression(float *h_suppressed,float *Gxy,float *h_image,int h_im_width,int h_im_height,float *h_gx_mask,float *h_gy_mask,int h_m_w,float *h_Gxy_outimage,float **h_I_angle);
//void consolidated_convolveMagAngleSuppressionSort(float *h_suppressed,float *Gxy,float *h_image,int h_im_width,int h_im_height,float *h_gx_mask,float *h_gy_mask,int h_m_w,float *h_Gxy_outimage,float **h_I_angle,float *hyst);
void consolidated_convolveMagAngleSuppressionSort(float *h_image,int h_im_width,int h_im_height,float *h_gx_mask,float *h_gy_mask,int h_m_w,float **h_I_angle,float *h_hyst,float *h_gxy);
void freecudaHost(float *I_angle);

