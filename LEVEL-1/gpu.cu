#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include"cuda_runtime_api.h"
#include<math.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/sort.h>
#include <thrust/copy.h>

#include "gpu.h"


void freecudaHost(float *I_angle){
cudaFreeHost(I_angle);
}

__constant__ float *d_gx_mask;
__constant__ float *d_gy_mask;
__constant__ float *mask;


__shared__ float Nshared[BLOCKWIDTH+2*(g_w/2)][BLOCKWIDTH+2*(g_w/2)];
__device__
void prepNshared(float *image,int width,int height){

                int i,j;
                //NORTH
                if(blockIdx.x==0)
                {
                        if(threadIdx.x >= BLOCKWIDTH - g_w/2)
                        {
                                Nshared[threadIdx.x+g_w/2 - BLOCKWIDTH][threadIdx.y+g_w/2]=0;
                        }
                }
                else
                {
                        if(threadIdx.x >=BLOCKWIDTH - g_w/2)
                        {
                                i=threadIdx.x+(blockIdx.x-1)*blockDim.x;
                                j=threadIdx.y+blockIdx.y*blockDim.y;
                                Nshared[threadIdx.x+g_w/2 - BLOCKWIDTH][threadIdx.y+g_w/2]=
                                image[i*width+j];
                        }
                }

		//for south elements

                if(blockIdx.x==gridDim.x - 1)
                {
                        if(threadIdx.x < g_w/2)
                        {
                                 Nshared[threadIdx.x + BLOCKWIDTH + g_w/2][threadIdx.y+g_w/2]=0; //VKP:Initially theadIdx.x + BLOCKWIDTH - g_w/2
                        }
                }
                else
                {
                        if(threadIdx.x < g_w/2)
                        {
                                i= threadIdx.x +(blockIdx.x+1)*blockDim.x;
                                j=threadIdx.y+(blockIdx.y *blockDim.y);
                                Nshared[threadIdx.x + BLOCKWIDTH + g_w/2][threadIdx.y+g_w/2]=image[i*width+j];//VKP:Initially theadIdx.x + BLOCKWIDTH - g_w/2
                        }
                }
                //for west elements

                if(blockIdx.y==0)
                {
                        if(threadIdx.y>= BLOCKWIDTH-g_w/2)
                        {
                                Nshared[threadIdx.x +g_w/2][threadIdx.y+g_w/2 - BLOCKWIDTH]=0;
                        }
                }
                else
                {
                        if(threadIdx.y>= BLOCKWIDTH-g_w/2)
                        {
                                i= threadIdx.x +blockIdx.x *blockDim.x;
                                j = threadIdx.y+(blockIdx.y-1)*blockDim.y;
                                Nshared[threadIdx.x +g_w/2][threadIdx.y+g_w/2 - BLOCKWIDTH]=image[i*width+j];
                        }
                }


                //for east elements

                if(blockIdx.y==gridDim.y-1)
                {
                        if(threadIdx.y < g_w/2) //VKP:Initially BLOCKWIDTH-g_w/2
                        {
                                Nshared[threadIdx.x +g_w/2][threadIdx.y+g_w/2+BLOCKWIDTH]=0;
                        }
                }
                else
                {
                        if(threadIdx.y < g_w/2) //VKP:Initially BLOCKWIDTH-g_w/2
                        {
                                i=threadIdx.x+blockIdx.x*blockDim.x;
                                j = threadIdx.y +(blockIdx.y+1)*blockDim.y;
                                Nshared[threadIdx.x +g_w/2][threadIdx.y+g_w/2+BLOCKWIDTH]=image[i*width+j];
                        }
                }

                //for north west elements

                if(blockIdx.x==0 || blockIdx.y==0)
                {
                        if((threadIdx.x >= BLOCKWIDTH-g_w/2)&&(threadIdx.y >= BLOCKWIDTH - g_w/2))
                        {
                                Nshared[threadIdx.x+g_w/2-BLOCKWIDTH][threadIdx.y+g_w/2 - BLOCKWIDTH]=0;
                        }
                }
                else
                {
                        if((threadIdx.x >= BLOCKWIDTH-g_w/2)&&(threadIdx.y >= BLOCKWIDTH - g_w/2))
                        {
                                i= threadIdx.x+(blockIdx.x -1)*blockDim.x;
                                j = threadIdx.y +(blockIdx.y -1)*blockDim.y;
                                Nshared[threadIdx.x+g_w/2-BLOCKWIDTH][threadIdx.y+g_w/2 - BLOCKWIDTH]=image[i*width+j];
                        }
                }
                //for north east elements

                if((blockIdx.x==0) || (blockIdx.y == gridDim.y-1))
                {
                        if((threadIdx.x >= BLOCKWIDTH - g_w/2)&&(threadIdx.y <g_w/2))
                        {
                                Nshared[threadIdx.x +g_w/2-BLOCKWIDTH][threadIdx.y+g_w/2+BLOCKWIDTH]=0; //VKP:mistake corrected in Nshared[][*]
                        }
                }
                else
                {
                        if((threadIdx.x >= BLOCKWIDTH - g_w/2)&&(threadIdx.y <g_w/2))
                        {
                                i= threadIdx.x +(blockIdx.x -1)*blockDim.x;
                                j = threadIdx.y +(blockIdx.y +1)*blockDim.y;
                                Nshared[threadIdx.x +g_w/2 - BLOCKWIDTH][threadIdx.y+g_w/2+BLOCKWIDTH]=image[i*width+j] =image[i*width+j]; //VKP: mistake corrected in Nshared[][*]
                        }
                }

                //for south west elements

                if((blockIdx.x==gridDim.x-1)||(blockIdx.y==0))
                {
                        if((threadIdx.x<g_w/2)&&(threadIdx.y >=BLOCKWIDTH- g_w/2)) //VKP:there was a mistake with threadIdx.y condition. Check test.cu for mistake
                        {
                                Nshared[threadIdx.x+g_w/2+BLOCKWIDTH][threadIdx.y+g_w/2 - BLOCKWIDTH] =0; //VKP: you forgot to g_w/2
                        }
                }
                else
                {
                        if((threadIdx.x <g_w/2)&&(threadIdx.y>= BLOCKWIDTH-g_w/2))//VKP:there was a mistake with threadIdx.y condition. Check test.cu for mistake
                        {
                                i= threadIdx.x +(blockIdx.x +1)*blockDim.x;
                                j = threadIdx.y +(blockIdx.y -1)*blockDim.y;
                                Nshared[threadIdx.x+g_w/2+BLOCKWIDTH][threadIdx.y+g_w/2 - BLOCKWIDTH] =image[i*width+j]; //you forgot to g_w/2
                        }
                }

                //forsouth east elements

                if((blockIdx.x == gridDim.x -1) || (blockIdx.y == gridDim.y-1))
                {
                        if((threadIdx.x <g_w/2)&&(threadIdx.y <g_w/2))
                        {
                                Nshared[threadIdx.x +g_w/2+BLOCKWIDTH][threadIdx.y+g_w/2+BLOCKWIDTH]=0;
                        }
                }
                else
                {
                        if((threadIdx.x <g_w/2)&&(threadIdx.y <g_w/2))
                        {
                                i=threadIdx.x +(blockIdx.x+1)*blockDim.x;
                                j = threadIdx.y+(blockIdx.y+1)*blockDim.y;
                        Nshared[threadIdx.x +g_w/2+BLOCKWIDTH][threadIdx.y+g_w/2+BLOCKWIDTH]=image[i*width+j];
                        }
                }

                i= threadIdx.x+blockIdx.x*blockDim.x;
                j=threadIdx.y+blockIdx.y*blockDim.y;

                Nshared[threadIdx.x +(g_w/2)][threadIdx.y+(g_w/2)]=image[i*width+j];

                __syncthreads ();

}


//kernel for magAngle and suppression
__global__
void gpu_convolveSharedConsolidatedMagandAngleSuppress(float *image,int width,int height,float *d_Gxy_outimage,float *d_Igx_mask,float *d_Igy_mask,float *d_I_angle,float *d_suppressed){
        int i,j;
        float temp=0;
        float temp1=0;
        int tid1 = threadIdx.x+blockIdx.x*blockDim.x;
        int tid2= threadIdx.y+blockIdx.y*blockDim.y;
        if((threadIdx.x+blockIdx.x*blockDim.x)<height && (threadIdx.y + blockIdx.y*blockDim.y)<width)
        {
          prepNshared(image,width,height);

         for(i=0;i<g_w;i++)
          {
                for(j=0;j<g_w;j++)
                {
                        temp = temp+d_Igx_mask[i*g_w+j]*Nshared[i+threadIdx.x][j+threadIdx.y];
                        temp1 = temp1+d_Igy_mask[i*g_w+j]*Nshared[i+threadIdx.x][j+threadIdx.y];
                }
        }
        d_Gxy_outimage[tid1*width+tid2]=sqrt(temp*temp+temp1*temp1);
        d_suppressed[tid1*width+tid2]=sqrt(temp*temp+temp1*temp1);
        d_I_angle[tid1*width+tid2] = atan2(temp, temp1);
 	} 
}


__global__
void gpu_doubleThreshold(float *d_hyst,float *d_suppressed,int width,int height,float th_high,float th_low){

int tid1 = threadIdx.x+blockIdx.x*blockDim.x;
int tid2 = threadIdx.y+blockIdx.y*blockDim.y;

	if(tid1<height && tid2<width)
	{
		if (d_suppressed[tid1*width+tid2]>th_high)
		{
			d_hyst[tid1*width+tid2]=(float)255;
			d_suppressed[tid1*width+tid2]=(float)255;
		}
		else if (d_suppressed[tid1*width+tid2]<th_high && d_suppressed[tid1*width+tid2]>th_low)
		{
			d_hyst[tid1*width+tid2]=(float)125;
			d_suppressed[tid1*width+tid2]=(float)125;
		}
		else 
		{
			d_hyst[tid1*width+tid2]=0;
			d_suppressed[tid1*width+tid2]=0;
		}
	}
}

//edge linking

__global__
void gpu_edgeLinking(float *d_buffer,float *d_hyst,int width,int height){

int tid1 = threadIdx.x+blockIdx.x*blockDim.x;
int tid2 = threadIdx.y+blockIdx.y*blockDim.y;

        if(tid1<height && tid2<width)
	{	
		if(d_buffer[tid1*width+tid2]==(float)125)
		{	
			d_hyst[tid1*width+tid2]=(float)0;
			if(tid1-1>=0)
			{
                           	if(d_buffer[(tid1-1)*width +tid2]==(float)255)
				{
                                      d_hyst[tid1*width+tid2]=(float)255;
				}
			}
                        if(tid1+1<height)
			{
                              	if(d_buffer[(tid1+1)*width + tid2]==(float)255)
			      	{
                                   d_hyst[tid1*width+tid2]=(float)255;
				}	
			}
				
			//left and right (i,j-1) and (i,j+1)
			if(tid1-1>=0)
			{
                        	if(d_buffer[tid1*width + (tid2-1)]==(float)255)
				{
                                	d_hyst[tid1*width + tid2]=(float)255;
				}
			}
                        if(tid2+1<width)
			{
                        	if(d_buffer[tid1*width + (tid2+1)]==255)
				{
                                	d_hyst[tid1*width + tid2]=(float)255;
				}
			}
				
			//Diagonal Pixels (tid1-1,tid2-1) and (tid1+1,tid2+1)
			if((tid1-1) >=0 && (tid2-1) >=0)
			{
                        	if(d_buffer[tid1*width + (tid2-1)]==(float)255)
				{
                                	d_hyst[tid1*width + tid2]=(float)255;
				}
			}
                        if(tid1+1<height && (tid2+1) <width)
			{
                        	if(d_buffer[(tid1+1)*width + (tid2+1)]==(float)255)
				{
                                	d_hyst[tid1*width + tid2]=(float)255;
				}
			}
			
			//Anti-Diagonal Pixels (tid1-1,tid2+1) (tid1+1,tid2-1)
			if((tid1-1)>=0 && (tid2+1) <width)
			{
                        	if(d_buffer[(tid1-1)*width + (tid2+1)]==(float)255)
				{
                                	d_hyst[tid1*width + tid2]=(float)255;
				}
			}
                        if((tid1+1)<height && (tid2-1) >=0)
			{
                        	if(d_buffer[(tid1+1)*width + (tid2-1)]==(float)255)
				{
                                	d_hyst[tid1*width + tid2]=(float)255;
				}
			}
                  }
	}
}


//device shared kernel for nonmaximal suppression
__shared__ float Nshared1[BLOCKWIDTH+2*(apron/2)][BLOCKWIDTH+2*(apron/2)];
__device__
void prepNshared1(float *image,int width,int height){

                int i,j;
                //NORTH
                if(blockIdx.x==0)
                {
                        if(threadIdx.x >= BLOCKWIDTH - apron/2)
                        {
                                Nshared1[threadIdx.x+apron/2 - BLOCKWIDTH][threadIdx.y+apron/2]=0;
                        }
                }
                else
                {
                        if(threadIdx.x >=BLOCKWIDTH - apron/2)
                        {
                                i=threadIdx.x+(blockIdx.x-1)*blockDim.x;
                                j=threadIdx.y+blockIdx.y*blockDim.y;
                                Nshared1[threadIdx.x+apron/2 - BLOCKWIDTH][threadIdx.y+apron/2]=
                                image[i*width+j];
                        }
                }

                //for south elements

                if(blockIdx.x==gridDim.x - 1)
                {
                        if(threadIdx.x < apron/2)
                        {
                                 Nshared1[threadIdx.x + BLOCKWIDTH + apron/2][threadIdx.y+apron/2]=0; //VKP:Initially theadIdx.x + BLOCKWIDTH - apron/2
                        }
                }
                else
                {
                        if(threadIdx.x < apron/2)
                        {
                                i= threadIdx.x +(blockIdx.x+1)*blockDim.x;
                                j=threadIdx.y+(blockIdx.y *blockDim.y);
                                Nshared1[threadIdx.x + BLOCKWIDTH + apron/2][threadIdx.y+apron/2]=image[i*width+j];//VKP:Initially theadIdx.x + BLOCKWIDTH - apron/2
                        }
                }
                //for west elements
if(blockIdx.y==0)
                {
                        if(threadIdx.y>= BLOCKWIDTH-apron/2)
                        {
                                Nshared1[threadIdx.x +apron/2][threadIdx.y+apron/2 - BLOCKWIDTH]=0;
                        }
                }
                else
                {
                        if(threadIdx.y>= BLOCKWIDTH-apron/2)
                        {
                                i= threadIdx.x +blockIdx.x *blockDim.x;
                                j = threadIdx.y+(blockIdx.y-1)*blockDim.y;
                                Nshared1[threadIdx.x +apron/2][threadIdx.y+apron/2 - BLOCKWIDTH]=image[i*width+j];
                        }
                }


                //for east elements

                if(blockIdx.y==gridDim.y-1)
                {
                        if(threadIdx.y < apron/2) //VKP:Initially BLOCKWIDTH-apron/2
                        {
                                Nshared1[threadIdx.x +apron/2][threadIdx.y+apron/2+BLOCKWIDTH]=0;
                        }
                }
                else
                {
                        if(threadIdx.y < apron/2) //VKP:Initially BLOCKWIDTH-apron/2
                        {
                                i=threadIdx.x+blockIdx.x*blockDim.x;
                                j = threadIdx.y +(blockIdx.y+1)*blockDim.y;
                                Nshared1[threadIdx.x +apron/2][threadIdx.y+apron/2+BLOCKWIDTH]=image[i*width+j];
                        }
                }

                //for north west elements

                if(blockIdx.x==0 || blockIdx.y==0)
                {
                        if((threadIdx.x >= BLOCKWIDTH-apron/2)&&(threadIdx.y >= BLOCKWIDTH - apron/2))
                        {
                                Nshared1[threadIdx.x+apron/2-BLOCKWIDTH][threadIdx.y+apron/2 - BLOCKWIDTH]=0;
                        }
                }
                else
                {
                        if((threadIdx.x >= BLOCKWIDTH-apron/2)&&(threadIdx.y >= BLOCKWIDTH - apron/2))
                        {
                                i= threadIdx.x+(blockIdx.x -1)*blockDim.x;
                                j = threadIdx.y +(blockIdx.y -1)*blockDim.y;
                                Nshared1[threadIdx.x+apron/2-BLOCKWIDTH][threadIdx.y+apron/2 - BLOCKWIDTH]=image[i*width+j];
                        }
                }
//for north east elements

                if((blockIdx.x==0) || (blockIdx.y == gridDim.y-1))
                {
                        if((threadIdx.x >= BLOCKWIDTH - apron/2)&&(threadIdx.y <apron/2))
                        {
                                Nshared1[threadIdx.x +apron/2-BLOCKWIDTH][threadIdx.y+apron/2+BLOCKWIDTH]=0; //VKP:mistake corrected in Nshared1[][*]
                        }
                }
                else
                {
                        if((threadIdx.x >= BLOCKWIDTH - apron/2)&&(threadIdx.y <apron/2))
                        {
                                i= threadIdx.x +(blockIdx.x -1)*blockDim.x;
                                j = threadIdx.y +(blockIdx.y +1)*blockDim.y;
                                Nshared1[threadIdx.x +apron/2 - BLOCKWIDTH][threadIdx.y+apron/2+BLOCKWIDTH]=image[i*width+j] =image[i*width+j]; //VKP: mistake corrected in Nshared1[][*]
                        }
                }

                //for south west elements

                if((blockIdx.x==gridDim.x-1)||(blockIdx.y==0))
                {
                        if((threadIdx.x<apron/2)&&(threadIdx.y >=BLOCKWIDTH- apron/2)) //VKP:there was a mistake with threadIdx.y condition. Check test.cu for mistake
                        {
                                Nshared1[threadIdx.x+apron/2+BLOCKWIDTH][threadIdx.y+apron/2 - BLOCKWIDTH] =0; //VKP: you forgot to apron/2
                        }
                }
                else
                {
                        if((threadIdx.x <apron/2)&&(threadIdx.y>= BLOCKWIDTH-apron/2))//VKP:there was a mistake with threadIdx.y condition. Check test.cu for mistake
                        {
                                i= threadIdx.x +(blockIdx.x +1)*blockDim.x;
                                j = threadIdx.y +(blockIdx.y -1)*blockDim.y;
                                Nshared1[threadIdx.x+apron/2+BLOCKWIDTH][threadIdx.y+apron/2 - BLOCKWIDTH] =image[i*width+j]; //you forgot to apron/2
                        }
                }

                //forsouth east elements

                if((blockIdx.x == gridDim.x -1) || (blockIdx.y == gridDim.y-1))
                {
                        if((threadIdx.x <apron/2)&&(threadIdx.y <apron/2))
                        {
                                Nshared1[threadIdx.x +apron/2+BLOCKWIDTH][threadIdx.y+apron/2+BLOCKWIDTH]=0;
                        }
                }
                else
                {
                        if((threadIdx.x <apron/2)&&(threadIdx.y <apron/2))
                        {
                                i=threadIdx.x +(blockIdx.x+1)*blockDim.x;
                                j = threadIdx.y+(blockIdx.y+1)*blockDim.y;
                        Nshared1[threadIdx.x +apron/2+BLOCKWIDTH][threadIdx.y+apron/2+BLOCKWIDTH]=image[i*width+j];
                        }
                }

                i= threadIdx.x+blockIdx.x*blockDim.x;
                j=threadIdx.y+blockIdx.y*blockDim.y;

                Nshared1[threadIdx.x +(apron/2)][threadIdx.y+(apron/2)]=image[i*width+j];
 		__syncthreads ();

}             

//non maximal suppression

__global__
void gpu_nonmaximalsuppression(float *d_Gxy_outimage,float *d_suppressed,float *d_I_angle,int width,int height){
	float theta=0;
        float mag=0;
        float temp=0;
        float temp1=0;
        int tid1 = threadIdx.x+blockIdx.x*blockDim.x;
        int tid2= threadIdx.y+blockIdx.y*blockDim.y;
        if((threadIdx.x+blockIdx.x*blockDim.x)<height && (threadIdx.y + blockIdx.y*blockDim.y)<width)
        {
          prepNshared1(d_Gxy_outimage,width,height);

         theta=(180/M_PI)*d_I_angle[tid1*width+tid2];
                        if(theta<0)
                                theta+=(float)180;
                        mag=Nshared1[threadIdx.x+(apron/2)][threadIdx.y+(apron/2)];
                        if(theta>(157.5) || theta <=22.5) //Left and Right
                        {
                                if((tid2-1) >=0)
				{
                                  if(mag<Nshared1[threadIdx.x+(apron/2)][threadIdx.y+(apron/2) -1])
                        	  {         
			             d_suppressed[tid1*width+tid2]=(float)0;
				  }
				}
                                if((tid2+1)<width)
				{
                                        if(mag<Nshared1[threadIdx.x+(apron/2)][threadIdx.y+(apron/2)+1])
					{
                                                d_suppressed[tid1*width+tid2]=(float)0;
                        		}
				}

			}

                        if(theta>(22.5) && theta <=67.5) //Diagonal pixels
                        {
                                if((tid1-1)>=0 && (tid2-1) >=0)
				{
                                        if(mag<Nshared1[threadIdx.x+(apron/2)-1][threadIdx.y+(apron/2)-1])
                                	{ 
				              d_suppressed[tid1*width+tid2]=(float)0;
					}
				}
                                if((tid1+1)<height && (tid2+1) <width)
				{
                                        if(mag<Nshared1[threadIdx.x+(apron/2)+1][threadIdx.y+(apron/2)+1])
					{
                                                d_suppressed[tid1*width+tid2]=(float)0;
					}
				}
                        }

                        if(theta>(67.5) && theta <= 112.5) //top and bottom. 
                        {
                                if((tid1-1)>=0)
				{
                                        if(mag<Nshared1[threadIdx.x+(apron/2)-1][threadIdx.y+(apron/2)])
                        		{          
			              		d_suppressed[tid1*width+tid2]=(float)0;
					}
				}
                                if((tid1+1)<height)
				{
                                        if(mag<Nshared1[threadIdx.x+(apron/2)+1][threadIdx.y+(apron/2)])
                                	{ 
				               d_suppressed[tid1*width+tid2]=(float)0;
					}
				}
                        }
                        if(theta>(112.5) && theta <= 157.5) //Anti Diagonal.
                        {
                                if((tid1+1)<height && (tid2-1)>=0)
				{
                                        if(mag<Nshared1[threadIdx.x+(apron/2)+1][threadIdx.y+(apron/2)-1])
					{
                                                d_suppressed[tid1*width+tid2]=(float)0;
					}
				}
                                if((tid1-1) >=0 &&(tid2+1)<=width)
				{
                                        if(mag<Nshared1[threadIdx.x+(apron/2)-1][threadIdx.y+(apron/2)+1])
					{
                                                d_suppressed[tid1*width+tid2]=(float)0;
					}
				}


                        }
        }
	
}


void consolidated_convolveMagAngleSuppressionSort(float *h_image,int h_im_width,int h_im_height,float *h_gx_mask,float *h_gy_mask,int h_m_w,float **h_I_angle,float *h_hyst,float *h_gxy){
float *d_image,*d_Gxy_outimage,*d_suppressed,*d_Igx_mask,*d_Igy_mask,*d_I_angle,*d_hyst;
float transfer_time=0;
float time1=0;
float time2=0;

cudaMallocHost((void **)h_I_angle,sizeof(float)*h_im_width*h_im_height);


cudaEvent_t start,stop;
cudaEventCreate(&start);
cudaEventCreate(&stop);

//Allocate memory for device pointers
cudaMalloc((void **)&d_image,sizeof(float)*h_im_width*h_im_height);
cudaMalloc((void **)&d_Gxy_outimage,sizeof(float)*h_im_width*h_im_height);
cudaMalloc((void **)&d_I_angle,sizeof(float)*h_im_width*h_im_height);
cudaMalloc((void **)&d_suppressed,sizeof(float)*h_im_width*h_im_height);
cudaMalloc((void **)&d_Igx_mask,sizeof(float)*h_m_w*h_m_w);
cudaMalloc((void **)&d_Igy_mask,sizeof(float)*h_m_w*h_m_w);
cudaMalloc((void **)&d_hyst,sizeof(float)*h_im_width*h_im_height);

//cudaEventRecord(start,0);
//memcpy to device pointers
cudaMemcpy(d_image,h_image,sizeof(float)*h_im_width*h_im_height,cudaMemcpyHostToDevice);
cudaMemcpy(d_Igx_mask,h_gx_mask,sizeof(float)*h_m_w*h_m_w,cudaMemcpyHostToDevice);
cudaMemcpy(d_Igy_mask,h_gy_mask,sizeof(float)*h_m_w*h_m_w,cudaMemcpyHostToDevice);
//cudaEventRecord(stop,0);
//cudaEventSynchronize(stop);
//cudaEventElapsedTime(&transfer_time,start,stop);


//cudaEventRecord(start,0);
dim3 dimGrid(ceil((float)h_im_height/(float)BLOCKWIDTH),ceil((float)h_im_width/(float)BLOCKWIDTH),1);
dim3 dimBlock(16,16,1);
gpu_convolveSharedConsolidatedMagandAngleSuppress<<<dimGrid,dimBlock>>>(d_image,h_im_width,h_im_height,d_Gxy_outimage,d_Igx_mask,d_Igy_mask,d_I_angle,d_suppressed);
cudaMemcpy(h_gxy,d_Gxy_outimage,sizeof(float)*h_im_width*h_im_height,cudaMemcpyDeviceToHost);
//cudaEventRecord(stop,0);

//cudaEventRecord(start,0);
gpu_nonmaximalsuppression<<<dimGrid,dimBlock>>>(d_Gxy_outimage,d_suppressed,d_I_angle,h_im_width,h_im_height);
//cudaEventRecord(stop,0);
//cudaEventSynchronize(stop);
//cudaEventElapsedTime(&transfer_time1,start,stop);
//printf("\nnonmaximal: %f",transfer_time1);

//cudaEventRecord(start,0);
cudaMemcpyAsync(*h_I_angle,d_I_angle,sizeof(float)*h_im_height*h_im_width,cudaMemcpyDeviceToHost);
//cudaMemcpy(h_suppressed,d_suppressed,sizeof(float)*h_im_width*h_im_height,cudaMemcpyDeviceToHost);
//cudaEventRecord(stop,0);
//cudaEventSynchronize(stop);
//cudaEventElapsedTime(&time2,start,stop);
//printf("\nd2H: %f",time2);


thrust::device_ptr<float> thr_d(d_suppressed);
thrust::device_vector<float>d_supp_vec(thr_d,thr_d+(h_im_height*h_im_width));
thrust::host_vector<float>h_supp_vec(h_im_height*h_im_width);
thrust::sort(d_supp_vec.begin(),d_supp_vec.end());

//getting high and low threshold

float th_high = d_supp_vec[(int)(0.9*h_im_height*h_im_width)];
float th_low =d_supp_vec[(int)(0.1*h_im_height*h_im_width)];

gpu_doubleThreshold<<<dimGrid,dimBlock>>>(d_hyst,d_suppressed,h_im_width,h_im_height,th_high,th_low);

//edge linking
gpu_edgeLinking<<<dimGrid,dimBlock>>>(d_suppressed,d_hyst,h_im_width,h_im_height);

//copy hyst image to host
cudaMemcpy(h_hyst,d_hyst,sizeof(float)*h_im_height*h_im_width,cudaMemcpyDeviceToHost);


cudaFree(d_image);
cudaFree(d_I_angle);
cudaFree(d_Igx_mask);
cudaFree(d_Igy_mask);
cudaFree(d_Gxy_outimage);
cudaFree(d_suppressed);
cudaFree(d_hyst);
printf("return to host");
}

